# Conversion API

Rest api to convert miles to km\*, pounds to kg\*, and fahrenheit to celsius*. 
\*: and vice versa.

## Getting Started

The only way I have managed to get the project to work is when it is run as a java app on Spring Tool Suite. I tried (after the deadline) to get it to run in a docker container, but could not get it to work.

## What it does

The app accepts string inputs as part of the URI, converts them to floats if possible, and returns the converted value as a string rounded to 2 decimal places.

```
http://localhost:8080/conversions/pounds-to-kg/3.14 ==> 1.42
http://localhost:8080/conversions/kg-to-pounds/3.14 ==> 6.92
http://localhost:8080/conversions/miles-to-km/1 ==> 1.61
http://localhost:8080/conversions/km-to-miles/1.6093 ==> 1.00
http://localhost:8080/conversions/celsius-to-fahrenheit/20 ==> 68.00
http://localhost:8080/conversions/fahrenheit-to-celsius/68 ==> 20.00
```

## Notes

I did not manage to make a frontend for this project, or write many meaningful tests, as I spent a lot of time learning spring and *trying* to get docker to work. In general I struggled a lot as I hadn't used any of these technologies before.
I feel I learnt a lot and will complete the project when I have enough time to read up on all the technologies beforehand. Thank you for the opportunity.