package charl.conversions.calculations;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ConversionControllerTest {

	ConversionController conversionController;
	
	@Before
	public void setUp() {
		conversionController = new ConversionController();
	}
	
	@Test
	public void milesToKm_PositiveInteger() {

		conversionController.milesToKm("1234");
		
		assertEquals("1985.93", conversionController.milesToKm("1234"));
		assertFalse("1985.92".equals(conversionController.milesToKm("1234")));
		assertFalse("1985.94".equals(conversionController.milesToKm("1234")));
	}
	
	@Test
	public void milesToKm_NegativeInteger() {
		
		assertTrue("Value could not be converted".equals(conversionController.milesToKm("-1234")));
	}
	
	@Test
	public void milesToKm_ZeroInteger() {
		conversionController.milesToKm("0");
		
		assertEquals("0.00", conversionController.milesToKm("0"));
	}
		
	@Test
	public void milesToKm_NonInteger() {
	    	
		assertTrue("Value could not be converted".equals(conversionController.milesToKm("-1234")));
	}
	
	@Test
	public void kmToMiles_PositiveInteger() {

		conversionController.kmToMiles("1234");
		
		assertEquals("766.77", conversionController.kmToMiles("1234"));
		assertFalse("766.76".equals(conversionController.kmToMiles("1234")));
		assertFalse("766.78".equals(conversionController.kmToMiles("1234")));
	}
	
	@Test
	public void kmToMiles_NegativeInteger() {
		
		assertTrue("Value could not be converted".equals(conversionController.kmToMiles("-1234")));
	}
	
	@Test
	public void kmToMiles_ZeroInteger() {
		conversionController.kmToMiles("0");
		
		assertEquals("0.00", conversionController.kmToMiles("0"));
	}
		
	@Test
	public void kmToMiles_NonInteger() {
	    	
		assertTrue("Value could not be converted".equals(conversionController.kmToMiles("-1234")));
	}

}
