package charl.conversions.calculations;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConversionController {
	
	@RequestMapping("/conversions/pounds-to-kg/{pounds}")
	public String poundsToKg(@PathVariable String pounds) {
		Float floatPounds = stringToFloat(pounds);
		if (floatPounds == null || floatPounds<0) {
			return "Value could not be converted";
		}
		float floatKg = floatPounds/2.2046226218f;
		String kgString = floatToString(floatKg);
		return kgString;
	}
	
	@RequestMapping("/conversions/kg-to-pounds/{kg}")
	public String kgToPounds(@PathVariable String kg) {
		Float floatKg = stringToFloat(kg);
		if (floatKg == null || floatKg<0) {
			return "Value could not be converted";
		}
		float floatPounds = floatKg*2.2046226218f;
		String poundsString = floatToString(floatPounds);
		return poundsString;
	}

	
	@RequestMapping("/conversions/miles-to-km/{miles}")
	public String milesToKm(@PathVariable String miles) {
		Float floatMiles = stringToFloat(miles);
		if (floatMiles == null || floatMiles<0) {
			return "Value could not be converted";
		}
		float floatKm = floatMiles*1.60934f;
		String kmString = floatToString(floatKm);
		return kmString;
	}
	
	@RequestMapping("/conversions/km-to-miles/{km}")
	public String kmToMiles(@PathVariable String km) {
		Float floatKm = stringToFloat(km);
		if (floatKm == null || floatKm<0) {
			return "Value could not be converted";
		}
		float floatMiles = floatKm/1.60934f;
		String milesString = floatToString(floatMiles);
		return milesString;
	}
	
	@RequestMapping("/conversions/celsius-to-fahrenheit/{celsius}")
	public String celsiusToFahrenheit(@PathVariable String celsius) {
		float floatCelsius = stringToFloat(celsius);
		float floatFahrenheit = floatCelsius*1.8f + 32.0f;
		String fahrenheitString = floatToString(floatFahrenheit);
		return fahrenheitString;
	}
	
	@RequestMapping("/conversions/fahrenheit-to-celsius/{fahrenheit}")
	public String fahrenheitToCelsius(@PathVariable String fahrenheit) {
		float floatFahrenheit = stringToFloat(fahrenheit);
		float floatCelsius = (floatFahrenheit - 32.0f)/1.8f;
		String celsiusString = floatToString(floatCelsius);
		return celsiusString;
	}
	
	private Float stringToFloat(String stringValue) {
		try {
			Float floatPounds = Float.valueOf(stringValue.trim()).floatValue();
			return floatPounds;
		} 
		catch(Exception e){
			System.out.println(e);
		}
		return null;
	}



	private String floatToString(float floatValue) {
		String poundsString = String.format("%.2f", floatValue);
		return poundsString;
	}
}
